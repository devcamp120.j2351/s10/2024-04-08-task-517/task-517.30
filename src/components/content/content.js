import { Component } from "react";
import Input from "./input/input";
import Output from "./output/output";

class Content extends Component {
    constructor(props){
        super(props);
        this.state = {
            inputTextMessage : "Nhập gì đó vào đây",
            outputMessage: [],
            displayLike: false
        }
    }

    saveInputText = (message) =>{
        this.setState({
            inputTextMessage: message,
        })
    }

    outputState = () => {
        console.log (this.state.inputTextMessage)
        this.setState({
            outputMessage: [...this.state.outputMessage, this.state.inputTextMessage],
            displayLike: true
        })
    }

    render() {
        return (
            <>
                <Input 
                    inputTextMessageProp = {this.state.inputTextMessage}
                    saveInputTextProp = {this.saveInputText}
                    outputStateProp = {this.outputState}
                />
                <Output 
                    outputTextMessageProp = {this.state.outputMessage}
                    displayLikeProp = {this.state.displayLike}
                />
            </>
        )
    }
}

export default Content;