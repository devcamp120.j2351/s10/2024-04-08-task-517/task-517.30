import { Component } from "react";

class Input extends Component {

    onInputChangeHandler = (event) => {
        //console.log("Giá trị của ô input");
        //console.log(event.target.value);
        this.props.saveInputTextProp(event.target.value);
    }

    onButtonClickHandler = () => {
        console.log("Nút gửi thông điệp được bấm");
        this.props.outputStateProp();
    }

    render() {
        return (
            <>
                <div className="pt-4">
                    <label>Message cho bạn 12 tháng tới:</label>
                </div>
                <div>
                    <input onChange={this.onInputChangeHandler} className="form-control"
                    value = {this.props.inputTextMessageProp}
                    ></input>
                </div>
                <div>
                    <button className="btn btn-success mt-4" onClick={this.onButtonClickHandler}>Gửi thông điệp</button>
                </div>
            </>
        )
    }
}

export default Input;