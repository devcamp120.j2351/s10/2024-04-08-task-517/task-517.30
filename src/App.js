import "bootstrap/dist/css/bootstrap.min.css";

import Header from "./components/header/header";
import Content from "./components/content/content";

function App() {
  return (
    <div className="container text-center pt-3">
     
      <Header />

      <Content />
      
    </div>
  );
}

export default App;
